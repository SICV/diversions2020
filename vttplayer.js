function vttplayer (elt, subtitle) {
    if (typeof(elt) == "string") {
        elt = document.querySelector(elt);
    }
    if (subtitle == undefined) {
      subtitle=".subtitle"
    }
    // console.log("audioplayer", elt);
    var media = elt.querySelector("audio,video"),
        track = elt.querySelector("track"),
        div = typeof(subtitle) == "string" ? elt.querySelector(subtitle) : subtitle;
    console.log("vttplayer", track, div);
    track.addEventListener("cuechange", e=>{
      let cues = track.track.activeCues;
      console.log('cuechange', e.target, cues[0]);
      div.innerHTML = cues[0].text;
    });
}