import argparse, sys, os, shutil


ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
ap.add_argument("frompath")
ap.add_argument("topath")
args= ap.parse_args()

for line in args.input:
    i = line.strip().lower()
    if i:
        inpath = os.path.join(args.frompath, i+".contours.svg")
        if not os.path.exists(inpath):
            print ("missing {}".format(inpath))
        else:
            newpath = os.path.join(args.topath, i+".contours.svg")
            print ("{} -> {}".format(i, newpath))
            shutil.copy(inpath,newpath)
