import argparse
ap = argparse.ArgumentParser()
ap.add_argument("--start", type=int, default=1)
ap.add_argument("--total", type=int, default=100)
ap.add_argument("--input", default="pdf/Carmentis_Coloring_Book.pdf")
ap.add_argument("--output", default="pdf/Carmentis_Coloring_Book.50rotations.pdf")
args = ap.parse_args()
cmd = "pdftk {} cat".format(args.input)
for i in range(args.start-1, args.start-1+args.total):
    cmd += " {0} {0}east {0}south {0}west".format(i+1)
cmd += " output {}".format(args.output)
print (cmd)
