#!/usr/bin/env python3
import sys, os, asyncio, json, argparse
import aiohttp, aiohttp_jinja2, jinja2
from aiohttp import web
# from urllib.parse import urlparse, unquote as urlunquote, quote as urlquote
from sketch_annotate import Predictor
import base64, re

def parse_data_url (d):
    pat = r"^data:(.+?);base64,(.*)$"
    m = re.search(pat,d)
    if m:
        return m.groups()

async def recognize (request):
    # ws = web.WebSocketResponse()
    # ws_ready = ws.can_prepare(request)
    # if not ws_ready.ok:
    #     return aiohttp_jinja2.render_template(request.app['template'], request, {})
    data = await request.post()
    mime, data = parse_data_url(data['image'])
    data = base64.b64decode(data)
    path = "tmp.jpg"
    with open (path, "wb") as f:
        f.write(data)
    # print ("data", mime, data)
    res = request.app['model'].predict_sketch_category(path)
    return web.json_response(res)
    # return web.Response(text="ok")

async def save (request):
    # ws = web.WebSocketResponse()
    # ws_ready = ws.can_prepare(request)
    # if not ws_ready.ok:
    #     return aiohttp_jinja2.render_template(request.app['template'], request, {})
    data = await request.post()
    if 'text' in data:
        with open(data['path'], "w") as f:
            f.write(data['text'])
    return web.json_response({'msg': 'ok'})


async def setup(app):
    app['model'] = Predictor()

# async def cleanup_background_tasks(app):
#     app['stdin_listener'].cancel()
#     await app['stdin_listener']

def main ():
    ap = argparse.ArgumentParser("")
    ap.add_argument("--host", default="localhost")
    ap.add_argument("--port", type=int, default=8080)
    # ap.add_argument("--static", nargs=2, default=None, action="append")
    # ap.add_argument("--template", default="pipeserver.html", help="Template for web page to serve")
    args = ap.parse_args()

    # On Windows, the default event loop is SelectorEventLoop which does not support run_in_executor. ProactorEventLoop should be used instead.
    # if sys.platform == 'win32':
    #     loop = asyncio.ProactorEventLoop()
    #     asyncio.set_event_loop(loop)

    app = web.Application()
    # template_path, app['template'] = os.path.split(args.template)
    #jinja_env = aiohttp_jinja2.setup(
    #    app, loader=jinja2.FileSystemLoader(template_path))
    # jinja_env.filters['datetimeformat'] = format_datetime

    # setup routes
    app.add_routes([web.post('/recognize', recognize)])
    app.add_routes([web.post('/save', save)])
    app.router.add_static('/','.',show_index=True)
    app.on_startup.append(setup)
    # app.on_cleanup.append(cleanup_background_tasks)
    web.run_app(app, host=args.host, port=args.port)

if __name__ == "__main__":
    main()
