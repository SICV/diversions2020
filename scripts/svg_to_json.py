#!/usr/bin/env python3
#!/usr/bin/env python3
import sys, re, json
from xml.etree import ElementTree as ET
from svg.path import parse_path, CubicBezier, Move, Line
import argparse

ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
args= ap.parse_args()

t = ET.parse(args.input)
data_out = []
for pl in t.findall(".//{http://www.w3.org/2000/svg}polyline"):
    data = pl.attrib.get("points").strip()
    data = re.split(r"\s+", data)
    data = [pair.split(",") for pair in data]
    data_out.append([{'x': int(x), 'y': int(y)} for x, y in data])
for p in t.findall(".//{http://www.w3.org/2000/svg}path"):
    path = parse_path(p.attrib.get("d").strip())
    for i in path:
        data_out.append([{'x': float(x.end.real), 'y': float(x.end.imag)} for x in path])
        # if type(i) == Move:
        #     print ("move", i.end)
        # elif type(i) == Line:
        #     print ("line", i.end)
        # elif type(i) == CubicBezier:
        #     print ("bezier", i.end)
print (json.dumps(data_out), file=args.output)


