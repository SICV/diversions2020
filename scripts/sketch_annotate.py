#!/usr/bin/env python3
import numpy as np
import os
import sys
# EDIT HERE: specify your caffe location.
# caffe_root = "/home/jalayrac/src/caffe"
# sys.path.insert(0, caffe_root+'/python')
import caffe
import time


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()

class Predictor (object):
    def __init__(self, sketch_pred_model = 'models/googlenet_deploy.prototxt', pretrained_file = 'models/finetune_googledraw_iter_360000.caffemodel'):
        # Set CPU/GPU mode (replace by caffe.set_mode_gpu() for GPU usage).
        caffe.set_mode_cpu()
        self.sketch_net_pred = caffe.Net(sketch_pred_model, pretrained_file, caffe.TEST)
        self.output_layer_pred = 'loss3/classifier_s'
        self.transformer = caffe.io.Transformer({'data': np.shape(self.sketch_net_pred.blobs['data'].data)})
        self.transformer.set_mean('data', np.array([104, 117, 123]))
        self.transformer.set_transpose('data',(2,0,1))
        self.transformer.set_channel_swap('data', (2,1,0))
        self.transformer.set_raw_scale('data', 255.0)
        file_list_class = 'models/class_list.txt'
        self.class_list = []
        with open(file_list_class, 'r') as f:
            for line in f:
                self.class_list.append(line.rstrip())

    def predict_sketch_category(self, path_img):
        """Predicts category of a sketch.

        Prints the top 5 prediction with their probability score.
        
        Args:
            path_img: Path to the image containing the sketch (png, jpg).
        """            
        
        start_time = time.time()
        
        # Load the image and preprocess it.
        sketch_in = (self.transformer.preprocess('data', caffe.io.load_image(path_img)))
        sketch_in = np.reshape([sketch_in], np.shape(self.sketch_net_pred.blobs['data'].data))

        # Forward pass in the network.
        query = self.sketch_net_pred.forward(data=sketch_in)
        query = np.copy(query[self.output_layer_pred])
        X_query = np.array(query.ravel())

        # Convert to probability with softmax function.
        probabilities = softmax(X_query)

        # Display the predictions.
        top_indexes = np.argsort(-X_query)
        duration_time = time.time()-start_time
        # print (time.time() - start_time, "seconds")
        ret = {}
        # {
        #     "_classes": ["class1", "class2", "class3"],
        #     "class1": {
        #         "score": 0.2342
        #     },
        # }
        ret['_classes'] = classes = []
        for ti in top_indexes[:7]:
        # for ti in top_indexes:
            k = self.class_list[ti]
            classes.append(k)
            ret[k] = {"score": float(probabilities[ti])}
        return ret
    # print("%s: %s (%0.3f), %s (%0.2f), %s (%0.2f), %s (%0.2f), %s (%0.2f) (served in %0.3f s)" %
    #       (os.path.basename(path_img),
    #        class_list[top_indexes[0]], probabilities[top_indexes[0]],
    #        class_list[top_indexes[1]], probabilities[top_indexes[1]],
    #        class_list[top_indexes[2]], probabilities[top_indexes[2]],
    #        class_list[top_indexes[3]], probabilities[top_indexes[3]],
    #        class_list[top_indexes[4]], probabilities[top_indexes[4]],
    #        duration_time))

if __name__ == "__main__":
    import argparse, json
    ap = argparse.ArgumentParser("")
    ap.add_argument("path", nargs="+")
    args = ap.parse_args()

    model = Predictor()

    for path in args.path:
        print (path)
        res = model.predict_sketch_category(path)
        print(json.dumps(res, indent=2))

        # first = True
        # script = ""
        # for label, prob in res:
        #     if first:
        #         script += "is it a {0}?".format(label)
        #     else:
        #         script += " or, is it a {0}?".format(label)
        #     first = False
        # print (script)
        # with open("script.txt", "w") as f:
        #     print(script, file=f)
        # os.system("festival --tts script.txt")
