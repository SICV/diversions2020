import csv, sys, json, argparse, os

ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
ap.add_argument("path")
args = ap.parse_args()

reader = csv.DictReader(args.input)
for row in reader:
    outpath = os.path.join(args.path, row['inventoryNb']+".meta.json")
    print ("writing {}".format(outpath))
    with open(outpath, "w") as fout:
        print(json.dumps(row, indent=2), file=fout)
