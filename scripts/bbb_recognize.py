import argparse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
from sketch_annotate import Predictor


ap = argparse.ArgumentParser()
ap.add_argument("--url", default="https://bbb.constantvzw.org/b/fem-rzq-99g")
ap.add_argument("--name", default="sketchybot")
ap.add_argument("--tmp", default="bbb_snapshot.png")
args = ap.parse_args()

print ("Initializing the predictor...")
model = Predictor()


#options = webdriver.ChromeOptions()
#options.add_argument('--ignore-certificate-errors')
#options.add_argument("--test-type")
#options.binary_location = "/usr/bin/chromium"
# driver = webdriver.Chrome(chrome_options=options)

print ("Starting the browser...")
driver = webdriver.Firefox()
driver.get(args.url)

sleep(5)

inp = driver.find_element_by_css_selector("input.join-form")
button = driver.find_element_by_css_selector("button.join-form")
inp.send_keys(args.name)
button.click()
sleep(5)
button = driver.find_element_by_xpath(".//button[@aria-label=\"Listen only\"]")
print (button)
button.click()
sleep(5)

chat = driver.find_element_by_id("message-input")
button = driver.find_element_by_xpath(".//button[@aria-label=\"Send message\"]")
# evt. to watch for other messages
chat_messages = driver.find_element_by_id("chat-messages")

def say (msg):
    chat.send_keys(msg)
    chat.send_keys(Keys.ENTER)

say("Hello from {}".format(args.name))
sleep(5)

# chat.send_keys("Goodbye")
# chat.send_keys(Keys.ENTER)
# sleep(3)

# g = driver.find_element_by_css_selector("svg g")

def symbol_for_score(score):
    if score >= 0.75:
        return "!!!"
    elif score >= 0.5:
        return "!!"
    elif score >= 0.25:
        return "!"
    elif score >= 0.1:
        return "?"
    elif score >= 0.01:
        return "??"
    else:
        return "???"
try:
    last_bytes = None
    while True:
        svg = driver.find_elements_by_css_selector("svg")[1]
        g = svg.find_element_by_css_selector("g")
        curbytes = g.screenshot_as_png
        if curbytes != last_bytes:
            with open(args.tmp, "wb") as f:
                f.write(curbytes)
            last_bytes = curbytes
            say("Let me see...")
            # todo: crop a bit the top / right side
            res = model.predict_sketch_category(args.tmp)
            guess = res['_classes'][0]
            score = res[guess]['score']
            sym = symbol_for_score(score)
            if sym.startswith("!"):
                say("It is a {}{}".format(guess, sym))
            else:
                say("Is it a {}{}".format(guess, sym))
            sleep(10)
        else:
            sleep(5)
finally:
    say("goodbye")
    driver.close()
#
