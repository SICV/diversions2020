#!/usr/bin/env python3
import html5lib, sys
from xml.etree import ElementTree as ET 


t = html5lib.parseFragment(sys.stdin, treebuilder = "etree", namespaceHTMLElements = False)
# replace images url with local image in ../images
for img in t.findall(".//img[@src]"):
    print (img, sys.stderr)

print (ET.tostring(t, method="html", encoding="unicode"), file=sys.stdout)

