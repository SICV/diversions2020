md=$(wildcard *.md)
mdhtml=$(md:%.md=%.html)
tablesvg=$(wildcard table/*.svg)
tablejson=$(tablesvg:%.svg=%.json)
whistlesvg=$(wildcard whistle/*.svg)
whistlejson=$(whistlesvg:%.svg=%.json)
tambourinesvg=$(wildcard tambourine/*.svg)
tambourinejson=$(tambourinesvg:%.svg=%.json)

korasvg=$(wildcard kora/*.svg)
korajson=$(korasvg:%.svg=%.json)


all: $(mdhtml)

index.md:
	wget https://pad.constantvzw.org/p/sketchy/export/txt -O - | \
	sed 's/^*#/#/' | \
	sed 1d > $@

json: $(tablejson) $(whistlejson) $(korajson) $(tambourinejson)

%.html: %.md
	pandoc --from markdown+implicit_figures --to html5 --template lib/template.html \
		--standalone  \
		--css styles.css \
		--toc \
		$< -o $@

# contours/%.json: contours/%.svg
# 	python3 scripts/contours_svg_to_json.py < $< > $@

%.json: %.svg
	python3 scripts/svg_to_json.py < $< > $@

table.txt:
	ls table/*.json > $@

whistle.txt:
	ls whistle/*.json > $@

kora.txt:
	ls kora/*.json > $@

kora_annotated.txt:
	ls kora/*.contours2.json > $@

whistle_annotated.txt:
	ls whistle/*.contours2.json > $@

mim-columns.txt:
	csvcut -n mim.csv > mim-columns.txt

whistle.csv:
	csvgrep -c objectName -r "^Whistle$" mim.csv > whistle.csv

whistle.inventoryNb.txt:
	csvcut -c inventoryNb whistle.csv > whistle.inventoryNb.txt


tambourine.csv:
	csvgrep -c objectName -r "^Tambourine" mim.csv > $@

tambourine.inventoryNb.txt: tambourine.csv
	csvcut -c inventoryNb tambourine.csv > $@

tambourine: tambourine.inventoryNb.txt
	mkdir -p tambourine
	python3 scripts/copyimages.py --input $< allitems_images_and_contours $@ 

tambourine.txt:
	ls tambourine/*.json > $@

tambourine_annotated.txt:
	ls tambourine/*contours2.json > $@

kora.csv:
	csvgrep -c objectName -r "^Kora" mim.csv > kora.csv

%.inventoryNb.txt: %.csv
	csvcut -c inventoryNb $< > $@

kora: kora.inventoryNb.txt
	python3 scripts/copyimages.py --input $< allitems_images_and_contours $@ 


whistle/%.csv: whistle/%.contours.json
	csvgrep -c objectName -r "^Whistle$" mim.csv > whistle.csv
