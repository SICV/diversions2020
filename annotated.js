function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function replace_extension (path, newext) {
    var m = path.match(/^(.+)\.contours2?\.json$/);
    if (m) {
        return m[1]+newext;
    } else {
        console.log(`replace_extensions did not match on: ${path}`)
        return path;
    }
}

function prediction_text (p) {
    var output = "",
        stmt = "Is it a ";
    p._classes.forEach(label => {
        var score = p[label].score,
            m = '';
        if (score >= 0.75) {
            m = "!!!";
            stmt = "It is a ";
        } else if (score >= 0.5) {
            m = "!!";
            stmt = "It is a ";
        } else if (score >= 0.25) {
            m = "!";
            stmt = "It is a ";
        } else if (score >= 0.1) {
            m = "?";
        } else if (score >= 0.01) {
            m = "??";
        } else {
            m = "???";
        }
        if (output) {
            output += ", ";
        }
        output += `${label}${m}`;
    })
    return `${stmt}${output}`;
}
var label_keys = ["inventoryNb", "objectName", "objectTitle", "objectCulture", "material", "dating", "geography"];
var current_labels,
    current_label_index = 0;

function fixurl(url) {
    return url.replace("carmentis.be/eMuseumPlus?", "carmentis.be/eMP/eMuseumPlus?");
}

function update_label () {
    // console.log("update_label");
    if (current_labels) {
        // console.log("LABELS", current_labels)
        var p = document.querySelector("#label"),
            key = label_keys[current_label_index],
            value = current_labels[key] || "unknown";
        var url = fixurl(current_labels['url']);
        console.log("carmentis url", url);
        p.innerHTML = `<a href="${url}" class="carmentis" target="_other">${key}: ${value}</a>`;
        if (++current_label_index >= label_keys.length) {
            current_label_index = 0;
        }
    }
}
window.setInterval(update_label, 5000);


async function go (labels_src) {
    var items = shuffle((await(await fetch(labels_src+"?"+new Date().toISOString())).text()).trim().split("\n")),
        i = 0;

    async function update_label () {
        var base = replace_extension(items[i],''),
            metadata_path = replace_extension(items[i],'.meta.json'),
            metadata = await(await fetch(metadata_path)).json();
        // console.log("GOT LABEL", metadata_path, metadata);
        var p = document.querySelector("#label");
        current_labels = metadata;
        // p.innerHTML = `${i+1}/${items.length}: ${base}`;
    }
    await update_label();
    // console.log("alltables", alltables, typeof(alltables));
    var ct = contour_tracer(document.querySelector("#contours"), {
        final_wait: 3000,
        points_at_a_time: 1,
        pre_draw_wait: 0,
        width: 500,
        height: 500,
        contours: items[i],
        shape: function (opts) {
            // console.log("shape", this, opts);
            this.pause();
            document.querySelector("#predictions").innerHTML = prediction_text(opts.shape.prediction);
            ct.play();
        },
        done: function () {
            // console.log("done");
            // next image
            document.querySelector("#predictions").innerHTML = '';
            window.setTimeout(x => {
                i += 1;
                if (i>=items.length) {
                    console.log("Completed ALL images, looping");
                    i = 0;
                }
                update_label();
                ct.contours(items[i]);

            }, 2500);
            return false;
        }
    });
    window.addEventListener("resize", ct.resize);
    ct.resize();

}
