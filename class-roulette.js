async function* makeTextFileLineIterator(fileURL) {
  const utf8Decoder = new TextDecoder('utf-8');
  const response = await fetch(fileURL);
  const reader = response.body.getReader();
  let { value: chunk, done: readerDone } = await reader.read();
  chunk = chunk ? utf8Decoder.decode(chunk) : '';

  const re = /\n|\r|\r\n/gm;
  let startIndex = 0;
  let result;

  for (;;) {
    let result = re.exec(chunk);
    if (!result) {
      if (readerDone) {
        break;
      }
      let remainder = chunk.substr(startIndex);
      ({ value: chunk, done: readerDone } = await reader.read());
      chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : '');
      startIndex = re.lastIndex = 0;
      continue;
    }
    yield chunk.substring(startIndex, result.index);
    startIndex = re.lastIndex;
  }
  if (startIndex < chunk.length) {
    // last line didn't end in a newline char
    yield chunk.substr(startIndex);
  }
}

async function getFile(urlOfFile, stock) {
  for await (let line of makeTextFileLineIterator(urlOfFile)) {
      stock.push(line);
  }
    testReady();
}

function testReady(){
    ready++;
    if(ready>1){
	if(coll){
	    collage();
	}else{
	    console.log("roulette");
	    roulette();
	}
    }
}

function roulette(){
    iSketch =  sketches[Math.floor(Math.random() * (sketches.length - 1))];  
    iMim =  mims[Math.floor(Math.random() * (mims.length - 1))];
    console.log(iSketch, iMim);
    setTimeout(function () {
	showSketch();
    }, 100);  
}

function collage(){
    if(!isPaused){
	slen = parseInt(document.getElementById('sketchlen').value);
	mlen = parseInt('-'+document.getElementById('mimlen').value);
	selSketch = sketches[Math.floor(Math.random() * (sketches.length - 1))];
	iSketch =  selSketch.slice(0,slen);  
	selMim = mims[Math.floor(Math.random() * (mims.length - 1))];
	iMim =  selMim.slice(mlen,-1).toLowerCase().replace(" ", "");
	document.getElementById("collage").innerHTML = '<span class="sketchblock">' + iSketch +  '</span><span class="mimblock">' + iMim + '</span>';
	collageName = iSketch+iMim;
	setTimeout(function () {
	    collage();
	}, 500);  
    }
}

function showSketch(){
    document.getElementById("mim").innerHTML = "";
    document.getElementById("sketch").innerHTML = iSketch.charAt(0).toUpperCase() + iSketch.slice(1) + "?";
    setTimeout(function () {
	showMim();
    }, 500);  
}

function showMim(){
    document.getElementById("mim").innerHTML = iMim + "";
    setTimeout(function () {
	roulette();
    }, 800);  
}

function processLine(str){
    var strOut = '<div class="line">' + str + '</div>';
    return strOut;
}


function pauseStart(){
  if(isPaused){
      isPaused =  false;
      document.querySelector('#pauseIt').textContent = 'Pause';
      collage();
  }else{
      isPaused = true;
      document.querySelector('#pauseIt').textContent = 'Restart';
  }
}

function view(){
    var redir = "collage.php?sketch="+selSketch+"&mim="+selMim+"&collagename="+collageName;
    window.location = encodeURI(redir);

}
