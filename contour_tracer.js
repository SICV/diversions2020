const wait = ms => new Promise(resolve => setTimeout(resolve, ms));
function fitinside (w, h, bw, bh) {
    var nw = bw,
        nh = nw * (h/w),
        ret;
    if (nh <= bh) {
        ret = {w: nw, h: nh};
    } else {
        nh = bh;
        nw = nh * (w/h);
        ret = {w: nw, h: nh};
    }
    // dx, dy for centering
    ret['x'] = (bw-nw)/2;
    ret['y'] = (bh-nh)/2;
    return ret;
}

function fitwidth (w, h, bw, bh) {
    return {w: bw, h: bw * (h/w), x: 0, y: 0};
}

var FIT_INSIDE = 1,
    FIT_WIDTH = 2;

function convert_from_old_to_new (all_contours) {
    return {
        contours: all_contours.map(contour_points => ({
            points: contour_points
        }))
    }
}

function contour_tracer (elt, opts) {
    opts = Object.assign({
        scaling: FIT_INSIDE,
        points_at_a_time: 1,
        pre_redraw_wait: 1000,
        final_wait: 5000,
        show_image_while_drawing: false,
        draw_fill_style: "white",
        redraw_stroke_style: "black",
        draw_stroke_style: 'rgba(0,0,0,1.00)',
        draw_line_width: 3,
        rand_stroke_style: false,
        rand_stroke_alpha: 0.50,
        redraw_fill_style: "white",
        redraw_stroke_style: "black",
        redraw_line_width: 3
    }, opts);
    var that = {},
        canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d"),
        elt_width,
        elt_height,
        max_scroll_height,
        item_image,
        naturalWidth,  // surrogates for image.naturalWidth
        naturalHeight,
        paused = false,
        pause_delay = 250,
        current_contour_data;

    elt.appendChild(canvas);

    function resize (e) {
        var bcr = elt.getBoundingClientRect(),
            w = bcr.width,
            h = bcr.height,
            img = item_image;

        elt_width = w;
        elt_height = h;
        // console.log("resize", w, h);
        var fit;
        naturalWidth = (img && img.naturalWidth) || opts.width;
        naturalHeight = (img && img.naturalHeight) || opts.height;
        if (opts.scaling == FIT_WIDTH) {
            fit = fitwidth(naturalWidth, naturalHeight, w, h);
        } else {
            fit = fitinside(naturalWidth, naturalHeight, w, h);
        }
        // console.log("fit", fit);
        canvas.width = fit.w;
        canvas.height = fit.h;
        canvas.style.left = fit.x+"px";
        canvas.style.top = fit.y+"px";
        max_scroll_height = canvas.height - h;

    }

    function randcolor () { return Math.round(Math.random()*255); }
    function randStrokeStyle () { return 'rgba('+randcolor()+','+randcolor()+','+randcolor()+','+opts.rand_stroke_alpha+')'; }
    function zero () { return 0; }
    function randSpeed () { return -1 + Math.random()*2; }

    async function start () {
        var resp = await fetch(opts.contours),
            contours_doc = await resp.json(),
            scale,
            min_y,
            max_y,
            scroll_y;

        // TODO: adapt to work with object style contour data
        //
        if (Array.isArray(contours_doc)) {
            // console.log("adapting OLD", contours_doc);
            contours_doc = convert_from_old_to_new(contours_doc);
            // console.log("NEW", contours_doc);
        }
        current_contour_data = contours_doc;

        paused = false;
        
        function adjust_scroll (y) {
            // ensure monotonicity...
            if (scroll_y === null || y < scroll_y) {
                scroll_y = y;
                if (scale) {
                    var screeny = scroll_y * scale;
                    var ideal_scroll_y = screeny - (elt_height/2);
                    ideal_scroll_y = Math.min(ideal_scroll_y, max_scroll_height);
                    if (window.scrollY != ideal_scroll_y) {
                        window.scrollTo({top: ideal_scroll_y});
                    }
                }           
            }
        }

        // determine min / max y values
        function check (x, y, shape_index, point_index) {
            if (min_y === undefined || y < min_y) { min_y = y; }
            if (max_y === undefined || y > max_y) { max_y = y; }
        }
        for (var shape_index=0; shape_index < contours_doc.contours.length; shape_index++) {
            var current_shape = contours_doc.contours[shape_index];
            check (current_shape.points[0].x, current_shape.points[0].y, shape_index, 0);
            for (var point_index = 1; point_index < current_shape.points.length; point_index++) {
                check (current_shape.points[point_index].x, current_shape.points[point_index].y, shape_index, point_index);
            }
        }

        while (1) {
            // FITINSIDE
            if (opts.scaling == FIT_INSIDE) {
                var fit = fitinside(naturalWidth, naturalHeight, elt_width, elt_height);
                scale = (fit.w / naturalWidth);
                if (opts.show_image_while_drawing) {
                    ctx.drawImage(item_image,0,0,canvas.width,canvas.height);
                }
            } else if (opts.scaling == FIT_WIDTH ) {
                scale = (canvas.width / naturalWidth);
                if (opts.show_image_while_drawing) {
                    ctx.drawImage(item_image,0,0,canvas.width,canvas.height);
                }   
            }
            if (!opts.show_image_while_drawing) {
                ctx.fillStyle = opts.draw_fill_style;
                ctx.strokeStyle = opts.draw_stroke_style;
                ctx.fillRect(0, 0, canvas.width, canvas.height);
            }
            ctx.strokeStyle= opts.draw_stroke_style;
            ctx.lineWidth = opts.draw_line_width;

            // DRAW CONTOURS
            // console.log("data", shapes, "scale", scale);
            scroll_y = null;
            ctx.save();
            ctx.scale(scale, scale);
            var points_counter = opts.points_at_a_time;

            for (var shape_index=0; shape_index < contours_doc.contours.length; shape_index++) {
                // console.log("drawing shape", (shape_index+1), "/", shapes.length);
                var current_shape = contours_doc.contours[shape_index];
                // ctx.strokeStyle = getShapeProperty(shape_index, 'strokeStyle', randStrokeStyle);
                if (opts.rand_stroke_style) {
                    ctx.strokeStyle = randStrokeStyle();
                }

                check (current_shape.points[0].x, current_shape.points[0].y, shape_index, 0);
                for (var point_index = 1; point_index < current_shape.points.length; point_index++) {
                    ctx.beginPath();
                    check (current_shape.points[point_index].x, current_shape.points[point_index].y, shape_index, point_index);
                    ctx.moveTo(current_shape.points[point_index-1].x, current_shape.points[point_index-1].y);
                    ctx.lineTo(current_shape.points[point_index].x, current_shape.points[point_index].y);
                    ctx.stroke();
                    if (--points_counter == 0) {
                        adjust_scroll(current_shape.points[point_index].y);
                        await wait(0);
                        points_counter = opts.points_at_a_time;
                    }       
                }
                // shape callback
                if (opts.shape) {
                    opts.shape.call(that, {i: shape_index, shape: current_shape});
                }
                while (paused) {
                    await wait(pause_delay);
                }
            }

            ctx.restore();
            await wait(opts.pre_redraw_wait);

            // console.log("draw black on white");
            // DRAW BLACK AND WHITE contours... fit to screen (print version)
            var fit = fitinside(naturalWidth, naturalHeight, elt_width, elt_height);
            scale = (fit.w / naturalWidth),

            ctx.save();
            ctx.fillStyle = opts.redraw_fill_style;
            ctx.strokeStyle = opts.redraw_stroke_style;
            ctx.lineWidth = opts.redraw_line_width;
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            if (opts.scaling == FIT_WIDTH) {
                ctx.translate(fit.x, fit.y);
            }
            ctx.scale(scale, scale);
            for (var shape_index=0; shape_index < contours_doc.contours.length; shape_index++) {
                // console.log("drawing shape", (shape_index+1), "/", shapes.length);
                var current_shape = contours_doc.contours[shape_index];
                // ctx.strokeStyle = getShapeProperty(shape_index, 'strokeStyle', randStrokeStyle);
                ctx.beginPath();
                ctx.moveTo(current_shape.points[0].x, current_shape.points[0].y);
                for (var point_index = 1; point_index < current_shape.points.length; point_index++) {
                    ctx.lineTo(current_shape.points[point_index].x, current_shape.points[point_index].y);
                }
                ctx.stroke();
            }

            ctx.restore();
            await wait(opts.final_wait);
            if (opts.done) {
                var keep_going = opts.done.call(that);
                if (!keep_going) {
                    return;
                }
            }
        }
    }

    if (opts.image) {
        item_image = new Image();
        item_image.src = opts.image;
        item_image.onload = function () {
            // console.log("image loaded");
            resize();
            start();    
        }   
    } else {
        resize();
        start();
    }
    that.resize = resize;

    that.contours = function (x) {
        opts.contours = x;
        start();
    }
    that.contour_data = function () {
        return current_contour_data;
    }
    that.pause = function () {
        paused = true;
    }
    that.play = function () {
        paused = false;
    }
    that.canvas = function () {
        return canvas;
    }
    that.context = function () {
        return ctx;
    }
    return that;
}

