# DiVersions2020

For the online installation of Sketchy Recognition, we plan a multiform online publication that:

#. Documents the experiences of the pianofabriek installation, 
#. Provides addition context in the form of text, software probes, and video, 
#. An invitation to participation through downloadable/printable PDF coloring book (photos of complete drawings can be uploaded to the site / archive), and an online discussion / collective drawing session. 

![](http://kurenniemi.activearchives.org/logbook/wp-content/uploads/2012/11/EK_Diary.png)



## Running the sketch annotator

 apt install python3-caffe-cpu

 python3 scripts/sketch_annotator...

