<?php
/*url params*/
$I_COLLAGE_NAME = $_GET['collagename'];
$INSTRU = "Bugle/";
$SKETCH = 'beer-mug/';
$COLLAGE_NAME = "Bugbee";

$COLLAGE_NAME = filter_var($_GET['collagename'], FILTER_SANITIZE_STRING);
$INSTRU = urldecode(filter_var($_GET['mim'], FILTER_SANITIZE_STRING))."/";
$SKETCH = urldecode(filter_var($_GET['sketch'], FILTER_SANITIZE_STRING))."/";

/*site params*/
$PUBLIC_ROOT = "/home/diversions/public_html/zigzag/sketchy-recognition/";
$INSTRU_ROOT = $PUBLIC_ROOT."instrumentsjs/";
$SKETCH_ROOT = $PUBLIC_ROOT."sketchesjs/";
$INSTRU_PATH = $INSTRU_ROOT.$INSTRU;
$SKETCH_PATH = $SKETCH_ROOT.$SKETCH;
$CACHE_DIR = "cache/";
$TXT_LIST = $CACHE_DIR.$COLLAGE_NAME.'.txt';
$DIR_OUT = $CACHE_DIR.$COLLAGE_NAME."js/";
//print $INSTRU_PATH."<br />".$SKETCH_PATH."<br />";
if(is_dir($DIR_OUT)){
  //echo $DIR_OUT." already exists.\n";
}else{
  mkdir($DIR_OUT);
  //echo $DIR_OUT." created.\n";
}


if (file_exists($TXT_LIST)) {
  //echo "File ".$TXT_LIST." exists.\n";
  $f = @fopen($TXT_LIST, "r+");
  if ($f !== false) {
    ftruncate($f, 0);
    fclose($f);
  }
} else {
  touch($TXT_LIST);
  //echo "File ".$TXT_LIST." created.\n";
}

function compute_ratio($anch, $comp){
  /* print_r($anch); */
  /* print_r($comp); */
  $ratio = array();
  $ratio['x'] = $comp->x - $anch->x;
  $ratio['y'] = $comp->y - $anch->y;
  return $ratio;
}

function diff_anchor($mes, $ratio){
  /* print_r($mes); */
  foreach($mes as $ms){
    $ms->x = $ms->x - $ratio['x'];
    $ms->y = $ms->y - $ratio['y'];
  }
  return $mes;
}

$dir_whistles = scandir($INSTRU_PATH);
/* print "whistles: "; */
/* print_r($dir_whistles); */
$dir_tables = scandir($SKETCH_PATH);
/* print "tables: "; */
/* print_r($dir_tables); */
if(count($dir_whistles) == 2){
  print "there is no contours for " . $INSTRU . "<br />";
}
//print "Nb instruments:" . count($dir_whistles)."<br />";
$tbl = fopen($TXT_LIST , "a") or die("Unable to open file! +r ".$TXT_LIST);

for($j = 0; $j < count($dir_tables); $j++){
  if(preg_match('/contours\.svg\.json/', $dir_whistles[$j])){
    $f1 = file_get_contents($INSTRU_PATH.$dir_whistles[$j]);
    $f2 = file_get_contents($SKETCH_PATH.$dir_tables[$j]);
    $ar_js1 = json_decode($f1);
    $ar_js2 = json_decode($f2);
    /* print_r($ar_js1[0]); */
    /* print_r($ar_js2[0]); */
    /* print count($ar_js1)." ".count($ar_js2)."\n"; */
    $lim1 = round(count($ar_js1)/2);
    $lim2 = round(count($ar_js2)/2);
    /* $lim1 = 1; */
    /* $lim2 = 1; */
    $out = array();
    $anchor = false;
    //    print $lim1." ".$lim2." <br />";
    for($i = 0; $i < $lim1; $i++){
      //var_dump($ar_js1);
      $out[] = $ar_js1[$i];
      $anchor = $ar_js1[$i];
      //print_r($anchor);
    }
    $ratio = compute_ratio($anchor[0], $ar_js2[0][1]);
    /* print_r($ratio); */
    /* die(); */
    for($i = 0; $i < $lim2; $i++){
      $out[] = diff_anchor($ar_js2[$i], $ratio);
    }
    /* for($i = (count($ar_js2)-1); $i >= $lim2; $i--){ */
    /*   $out[] = $ar_js2[$i]; */
    /* } */
    $js_out = json_encode($out);
    /* print $js_out; */
    $out_path = $DIR_OUT.$dir_tables[$j];
    $myfile = fopen($out_path, "w") or die("Unable to open file! +w ".$out_path);
    fwrite($myfile, $js_out);
    fclose($myfile);
    fwrite($tbl, $out_path."\n");
  }}
$repl_sketch = str_replace("/","",$SKETCH);
$repl_instru = str_replace("/","",$INSTRU);
$str_tmpl = file_get_contents('collage.view.tpl.html');
$str_tmpl = preg_replace('/<!--collage_name-->/',$COLLAGE_NAME,$str_tmpl);
$str_tmpl = preg_replace('/<!--sketch_name-->/',$repl_sketch,$str_tmpl);
$str_tmpl = preg_replace('/<!--mim_name-->/',$repl_instru,$str_tmpl);
$str_tmpl = preg_replace('/<!---txt_list-->/',$TXT_LIST,$str_tmpl);
print $str_tmpl;

?>